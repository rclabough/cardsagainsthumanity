﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CardsAgainstHumanity.Server.Data;
using CardsAgainstHumanity.Server.Logic.Interfaces;
using CardsAgainstHumanity.Shared.Extensions;

namespace CardsAgainstHumanity.Server.Logic.Game
{

    public interface IGameServiceFactory
    {
        GameService Get();
    }

    public class GameServiceFactory : IGameServiceFactory
    {
        private readonly ICardService _cardService;
        private readonly INotificationFactory _notificationFactory;

        public GameServiceFactory(ICardService cardService, INotificationFactory notificationFactory)
        {
            _cardService = cardService;
            _notificationFactory = notificationFactory;
        }

        public static GameService Instance;


        public GameService Get()
        {
            lock (typeof (IGameServiceFactory))
            {
                return Instance ?? (Instance = new GameService(_cardService, _notificationFactory));
            }
        }
    }

    public class GameService
    {
        private readonly ICardService _cardService;
        private readonly INotificationFactory _notificationFactory;
        private readonly int _keepalive;
        private static readonly ConcurrentDictionary<string, GameContainer> ActiveGames = new ConcurrentDictionary<string, GameContainer>();
        private bool cleanTime;

        public GameService(ICardService cardService, INotificationFactory notificationFactory)
        {
            _cardService = cardService;
            _notificationFactory = notificationFactory;
            _keepalive = 120; //2 hours
        }

        public string CreateGame()
        {
            var gameId = StringExtensions.RandomString(6);
            ActiveGames[gameId] = new GameContainer
            {
                Game = new Game(_cardService, _notificationFactory, gameId),
                Touch = DateTime.Now
            };
            return gameId;
        }

        public Game GetGame(string gameId)
        {
            var container = ActiveGames.Get(gameId);
            container.Touch = DateTime.Now;
            SweepAndSleep();
            return container.Game;
        }

        public void EndGame(string gameId)
        {
            GameContainer container;
            ActiveGames.TryRemove(gameId, out container);
        }


        private void SweepAndSleep()
        {
            if (!cleanTime)
            {
                return;
            }
            cleanTime = false;
            new Task(async () =>
            {
                await Sweep();
                Thread.Sleep(300000); //5 minutes
                cleanTime = true;
            }).Forget().Start();
        }

        private async Task Sweep()
        {
            await Task.Run(() =>
            {
                var deadTime = DateTime.Now.AddMinutes(-1*_keepalive);
                var deadGames = ActiveGames.Values.Where(k => k.Touch < deadTime).Select(k => k.Game.GameId).ToList();
                foreach (var deadGameId in deadGames)
                {
                    EndGame(deadGameId);
                }
            });
        }






        private class GameContainer
        {
            public Game Game { get; set; }
            public DateTime Touch { get; set; }
        }
    }
}

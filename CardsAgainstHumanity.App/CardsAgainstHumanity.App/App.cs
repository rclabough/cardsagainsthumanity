﻿using CardsAgainstHumanity.App.Pages;
using CardsAgainstHumanity.Plug.Controllers;
using Xamarin.Forms;

namespace CardsAgainstHumanity.App
{
    public class App : Application
    {

        public App()
        {
            // The root page of your application
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}

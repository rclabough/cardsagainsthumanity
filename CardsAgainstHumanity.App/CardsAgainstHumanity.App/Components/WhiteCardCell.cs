﻿using System;
using System.Linq;
using CardsAgainstHumanity.Shared.Models;
using Xamarin.Forms;

namespace CardsAgainstHumanity.App.Components
{
    public class WhiteCardCell : CardCell
    { 
        public delegate void PickButtonClicked(Guid cardId, int pickNumber);

        public event PickButtonClicked OnPickButtonClicked;

        public WhiteCardCell(WhiteCardModel model, int picks)
        {
            CardId = model.WhiteCardId;
            Value = model.Value;

            var buttonStack = new StackLayout();
            for (var i = 1; i < picks + 1; i++)
            {
                var button = new Button
                {
                    Text = "Pick " + i
                };
                var i1 = i;
                button.Clicked += (sender, args) => OnPickButtonClicked?.Invoke(CardId, i1);
                buttonStack.Children.Add(button);
            }

            Layout = new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Children =
                {
                    new Label
                    {
                        Text = Value
                    },
                    buttonStack
                }
            };
        }
    }
}

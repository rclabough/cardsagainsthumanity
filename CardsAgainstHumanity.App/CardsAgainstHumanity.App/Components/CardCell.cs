﻿using System;
using Xamarin.Forms;

namespace CardsAgainstHumanity.App.Components
{
    public abstract class CardCell 
    {
        public Guid CardId { get; set; }
        public string Value { get; set; }

        public Layout Layout { get; protected set; }
    }
}

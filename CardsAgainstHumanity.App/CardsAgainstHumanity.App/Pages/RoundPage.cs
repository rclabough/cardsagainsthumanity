﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CardsAgainstHumanity.App.Components;
using CardsAgainstHumanity.Plug.Controllers;
using CardsAgainstHumanity.Plug.SignalR;
using CardsAgainstHumanity.Shared.Extensions;
using CardsAgainstHumanity.Shared.GameProperties;
using CardsAgainstHumanity.Shared.Models;
using Xamarin.Forms;

namespace CardsAgainstHumanity.App.Pages
{
    public class RoundPage : ContentPage
    {
        private readonly Label _blackCardLabel;
        private readonly StackLayout _whiteCardStack;
        private PlayerRoundModel _roundModel;
        private IDictionary<Guid, WhiteCardModel> _whiteCardLookups;
        private WhiteCardModel[] _picks;
        private readonly Label _errorLabel;

        public RoundPage()
        {
            _blackCardLabel = new Label();
            _whiteCardStack = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.CenterAndExpand,
            };
            var submitButton = new Button
            {
                Text = "Submit Button"
            };
            _errorLabel = new Label
            {
                IsVisible = false,
                TextColor = Color.Red
            };
            submitButton.Clicked += SubmitButtonOnClicked;
            Content = new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Children =
                {
                    _blackCardLabel,
                    _errorLabel,
                    new ScrollView
                    {
                        Content = _whiteCardStack
                    },
                    submitButton
                }
            };
        }

        private async void SubmitButtonOnClicked(object sender, EventArgs eventArgs)
        {
            var model = new SubmitCardModel
            {
                CardIds = _picks.Select(k => k.WhiteCardId).ToArray(),
                GameId = GameProperties.Instance.GameId,
                PlayerId = GameProperties.Instance.PlayerId
            };
            await new RoundApi().Submit(model);
            OnRoundOver();
        }

        protected override async void OnAppearing()
        {
            var gameId = GameProperties.Instance.GameId;
            var playerId = GameProperties.Instance.PlayerId;
            var signalRService = SignalRClient.GetInstance(gameId);
            _roundModel = await new RoundApi().GetPlayerRound(gameId, playerId);
            _blackCardLabel.Text = Format(_roundModel);
            _picks = new WhiteCardModel[_roundModel.BlackCard.Pick];
            _whiteCardLookups = _roundModel.WhiteCards.ToConcurrentDictionary(k => k.WhiteCardId, v => v);
            foreach (var item in _roundModel.WhiteCards.Select(k => new WhiteCardCell(k, _roundModel.BlackCard.Pick)))
            {
                _whiteCardStack.Children.Add(item.Layout);
                item.OnPickButtonClicked += ItemOnOnPickButtonClicked;
            }
            
            (await signalRService).RoundOver += OnRoundOver;
            base.OnAppearing();
        }

        private void ItemOnOnPickButtonClicked(Guid cardId, int pickNumber)
        {
            try
            {
                _picks[pickNumber - 1] = _whiteCardLookups.Get(cardId);
                _blackCardLabel.Text = Format(_roundModel.BlackCard.FormattableValue, _roundModel.BlackCard.Pick, _picks);
            }
            catch (Exception ex)
            {
                _errorLabel.Text = $"You must select all of your picks!  (You need {pickNumber})";
            }
        }

        private void OnRoundOver()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    await Navigation.PopModalAsync();
                }
                finally
                {
                    await Navigation.PushModalAsync(new RoundLobbyPage());
                }
            });
        }

        private string Format(PlayerRoundModel model)
        {
            BlackCardModel blackCard = model?.BlackCard;
            if (blackCard == null)
            {
                return null;
            }
            WhiteCardModel[] whiteCards = (model?.WhiteCards ?? new List<WhiteCardModel>()).ToArray();
            return Format(blackCard?.FormattableValue, blackCard.Pick, model?.WhiteCards.ToArray());
        }

        private string Format(string formattableValue, int picks, WhiteCardModel[] cards)
        {
            var arr = new object[picks];
            for (var i = 0; i < picks; i++)
            {
                var value = "_____";
                var card = cards[i];
                if (card != null)
                {
                    value = card.Value;
                }
                arr[i] = value;
            }
            return string.Format(formattableValue, arr);
        }

    }
}

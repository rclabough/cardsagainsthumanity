﻿using System;
using CardsAgainstHumanity.Plug.Controllers;
using CardsAgainstHumanity.Shared.GameProperties;
using CardsAgainstHumanity.Shared.Models;
using Xamarin.Forms;

namespace CardsAgainstHumanity.App.Pages
{
    public class MainPage : ContentPage
    {
        private readonly Button _startButton;
        private readonly Button _showJoinButton;
        private readonly Button _joinButton;

        private readonly Label _gameIdLabel;
        private readonly Entry _gameIdText;
        private readonly Entry _playerIdText;

        public MainPage()
        {
            _startButton = new Button
            {
                Text = "Create Game",
            };
            _startButton.Clicked += StartButtonOnClicked;
            _showJoinButton = new Button
            {
                Text = "Join Game"
            };
            _showJoinButton.Clicked += ShowJoinButtonOnClicked;
            _gameIdLabel = new Label
            {
                IsVisible = false
            };
            _gameIdText = new Entry
            {
                Placeholder = "Game ID",
                IsVisible = false,
            };
            _playerIdText = new Entry
            {
                Placeholder = "Player Name",
                IsVisible = false
            };
            _joinButton = new Button()
            {
                Text = "Join Game",
                IsVisible = false
            };
            _joinButton.Clicked += JoinButtonOnClicked;

            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.Center,
                Children =
                {
                    _startButton,
                    _showJoinButton,
                    _joinButton,
                    _gameIdLabel,
                    _gameIdText,
                    _playerIdText
                }
            };
        }

        private void HideButtons()
        {
            _startButton.IsVisible = false;
            _showJoinButton.IsVisible = false;
        }

        private void ShowInputs()
        {
            _gameIdText.IsVisible = true;
            _playerIdText.IsVisible = true;
        }

        private void ShowJoinButtonOnClicked(object sender, EventArgs eventArgs)
        {
            HideButtons();
            ShowInputs();
            _joinButton.IsVisible = true;
        }
        
        private async void StartButtonOnClicked(object sender, EventArgs eventArgs)
        {
            HideButtons();
            var gameId = await new MatchApi().CreateGame();
            GameProperties.Instance.GameId = gameId;
            _gameIdLabel.IsVisible = true;
            _gameIdLabel.Text = gameId;
        }

        private async void JoinButtonOnClicked(object sender, EventArgs eventArgs)
        {
            var gameId = _gameIdText.Text;
            var playerId = _playerIdText.Text;
            var result = await new MatchApi().JoinGame(new JoinGameModel {GameId = gameId, PlayerId = playerId});

            GameProperties.Instance.GameId = gameId;
            GameProperties.Instance.PlayerId = playerId;
            await Navigation.PushModalAsync(new LobbyPage());
        }
    }
}

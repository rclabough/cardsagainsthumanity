﻿using System.Collections.ObjectModel;
using CardsAgainstHumanity.Plug.Controllers;
using CardsAgainstHumanity.Plug.SignalR;
using CardsAgainstHumanity.Shared.GameProperties;
using Xamarin.Forms;

namespace CardsAgainstHumanity.App.Pages
{
    public class LobbyPage : ContentPage
    {
        private Label _gameIdLabel;
        private ListView _currentPlayers;

        private readonly ObservableCollection<TextCell> _players;
        private SignalRClient _signalRClient;

        public LobbyPage()
        {
            
            _players = new ObservableCollection<TextCell>();
            _gameIdLabel = new Label()
            {
                Text = GameProperties.Instance.GameId
            };

            _currentPlayers = new ListView {ItemsSource = _players};
        }

        protected override async void OnAppearing()
        {
            _signalRClient = await SignalRClient.GetInstance(GameProperties.Instance.GameId);

            _signalRClient.GameReady += SignalRClientOnGameReady;

            var players = await new GameApi().Players(GameProperties.Instance.GameId);
            foreach (var player in players)
            {
                AddPlayer(player);
            }
            _signalRClient.PlayerAdded += AddPlayer;
            base.OnAppearing();
        }

        private void AddPlayer(string playerId)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                _players.Add(new TextCell { Text = playerId });
            });
        }

        private void SignalRClientOnGameReady(string message)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Navigation.PushModalAsync(new RoundPage());
            });
        }
    }
}

﻿using System;
using CardsAgainstHumanity.Plug.Controllers;
using CardsAgainstHumanity.Plug.SignalR;
using CardsAgainstHumanity.Shared.GameProperties;
using Xamarin.Forms;

namespace CardsAgainstHumanity.App.Pages
{
    public class RoundLobbyPage : ContentPage
    {
        private readonly StackLayout _scoreLayout;


        public RoundLobbyPage()
        {
            _scoreLayout = new StackLayout();

            Content = new StackLayout()
            {
                Children =
                {
                    new Label()
                    {
                        Text = "Waiting next for round to start..."
                    },
                    _scoreLayout
                }
            };
        }

        protected override async void OnAppearing()
        {
            var gameId = GameProperties.Instance.GameId;
            var isRoundOverTask = new RoundApi().IsRoundOver(gameId);
            var signalRService = await SignalRClient.GetInstance(gameId);
            signalRService.GameReady += SignalRServiceOnGameReady;
            if (await isRoundOverTask)
            {
                SendToNewRound();
            }
            var scores = await new GameApi().Scores(gameId);
            foreach (var item in scores.Scores)
            {
                _scoreLayout.Children.Add(new Label()
                {
                    Text = $"{item.PlayerId} : {item.Score}"
                });
            }
            base.OnAppearing();
        }

        private void SignalRServiceOnGameReady(string message)
        {
            SendToNewRound();
        }


        private void SendToNewRound()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Navigation.PushModalAsync(new RoundPage());
            });
        }
    }
}

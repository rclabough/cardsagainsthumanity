﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using CardsAgainstHumanity.Server.Logic.Game;
using CardsAgainstHumanity.Shared.Endpoints;
using CardsAgainstHumanity.Shared.Models;
using Newtonsoft.Json;

namespace CardsAgainstHumanity.Server.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MatchController : ApiController, IMatchEndpoint
    {
        private readonly GameService _gameService;

        public MatchController(GameService gameService)
        {
            _gameService = gameService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<string> CreateGame()
        {
            return await Task.Run(() => _gameService.CreateGame());
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<bool> JoinGame([FromBody]JoinGameModel model)
        {
            return await Task.Run(() =>
            {
                var game = _gameService.GetGame(model.GameId);
                game.AddPlayer(model.PlayerId);
                return game.IsRunning;
            });
        }



        [HttpGet]
        [AllowAnonymous]
        public async Task<bool> LeaveGame(string playerId, string gameId)
        {
            return await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                game.RemovePlayer(playerId);
                return true;
            });
        }
        
    }
}

﻿using CardsAgainstHumanity.Server.Data;
using CardsAgainstHumanity.Server.Logic.Game;
using CardsAgainstHumanity.Shared.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using CardsAgainstHumanity.Shared.Endpoints;
using Newtonsoft.Json;

namespace CardsAgainstHumanity.Server.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RoundController : ApiController, IRoundEndpoint
    {
        private readonly GameService _gameService;
        private readonly ICardService _cardService;

        public RoundController(GameService gameService, CardService cardService)
        { 
            _gameService = gameService;
            _cardService = cardService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<int> Create(string gameId)
        {
            return await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                var round = game.CreateRound();
                return round.RoundNumber;
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<PlayerRoundModel> GetPlayerRound(string gameId, string playerId)
        {
            return await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                var blackCard = _cardService.GetBlackCard(game.CurrentRound.BlackCardId);
                var model = new PlayerRoundModel
                {
                    GameId = gameId,
                    PlayerId = playerId,
                    RoundId = game.CurrentRound.RoundNumber,
                    BlackCard = new BlackCardModel
                    {
                        BlackCardId = blackCard.BlackCardId,
                        RawValue = blackCard.RawValue
                    },
                    WhiteCards =
                        game.Hands[playerId].CardsInHand.Select(k => _cardService.GetWhiteCard(gameId, k))
                            .Select(k => new WhiteCardModel
                            {
                                WhiteCardId = k.WhiteCardId,
                                Value = k.Value
                            })
                };
                return model;
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<string>> PlayersWhoSubmitted(string gameId)
        {
            return await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                return game.CurrentRound.PlayerSubmittedWhiteCards.Select(k => k.Key);
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<SubmissionModel> Submissions(string gameId)
        {
            return await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                var blackCard = _cardService.GetBlackCard(game.CurrentRound.BlackCardId);
                var blackCardModel = new BlackCardModel
                {
                    BlackCardId = blackCard.BlackCardId,
                    RawValue = blackCard.RawValue
                };
                var models = game.CurrentRound.PlayerSubmittedWhiteCards.Select(k => new PlayerSubmissionModel
                {
                    PlayerId = k.Key,
                    SubmittedAnswer =
                        string.Format(blackCardModel.FormattableValue,
                            k.Value.Select(j => _cardService.GetWhiteCard(gameId, j).Value).ToArray()),
                    Cards = k.Value.Select(j => _cardService.GetWhiteCard(gameId, j)).Select(j => new WhiteCardModel
                    {
                        Value = j.Value,
                        WhiteCardId = j.WhiteCardId
                    })
                });
                return new SubmissionModel
                {
                    BlackCard = new BlackCardModel
                    {
                        BlackCardId = blackCard.BlackCardId,
                        RawValue = blackCard.RawValue
                    },
                    Submissions = models
                };
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<HostRoundModel> GetHostRound(string gameId)
        {
            return await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                var blackCard = _cardService.GetBlackCard(game.CurrentRound.BlackCardId);
                var model = new HostRoundModel
                {
                    BlackCard = new BlackCardModel
                    {
                        BlackCardId = blackCard.BlackCardId,
                        RawValue = blackCard.RawValue
                    },
                    Players = game.Players
                };
                return model;
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<bool> IsRoundOver(string gameId)
        {
            return await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                return game?.CurrentRound != null
                       && game.CurrentRound.IsRoundOver();
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task End(string gameId)
        {
            await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                game?.CurrentRound?.ForceRoundOver();
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task Submit([FromBody]SubmitCardModel model)
        {
            await SubmitParsed(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task Submit(string model)
        {
            var modelParsed = JsonConvert.DeserializeObject<SubmitCardModel>(model);
            await SubmitParsed(modelParsed);
        }

        private async Task SubmitParsed(SubmitCardModel model)
        {
            await Task.Run(() =>
            {
                var game = _gameService.GetGame(model.GameId);
                game.SubmitCards(model.PlayerId, model.CardIds);
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task SubmitWinner([FromBody]PlayerModel model)
        {
            await Task.Run(() =>
            {
                var game = _gameService.GetGame(model.GameId);
                game.CurrentRound.SubmitWinner(model.PlayerId);
            });
        }
    }
}

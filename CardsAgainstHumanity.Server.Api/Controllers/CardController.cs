﻿using System;
using CardsAgainstHumanity.Server.Data;
using CardsAgainstHumanity.Server.Data.Cards;
using CardsAgainstHumanity.Server.Logic.Game;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using CardsAgainstHumanity.Shared.Endpoints;
using CardsAgainstHumanity.Shared.Models;
using Newtonsoft.Json;

namespace CardsAgainstHumanity.Server.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CardController : ApiController, ICardEndpoint
    {
        private readonly GameService _gameService;
        private readonly CardService _cardService;

        public CardController(GameService gameService, CardService cardService)
        {
            _gameService = gameService;
            _cardService = cardService;
        }

        [HttpGet]
        public async Task<BlackCardModel> BlackCard(string gameId, int roundNumber)
        {
            return await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                var blackCardId = game.Rounds[roundNumber].BlackCardId;
                var blackCard = _cardService.GetBlackCard(blackCardId);
                return new BlackCardModel
                {
                    BlackCardId = blackCard.BlackCardId,
                    RawValue = blackCard.RawValue
                };
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<Guid> SubmitCustomCard(string model)
        {
            return await Task.Run(() =>
            {
                var id = Guid.NewGuid();
                var dmodel = JsonConvert.DeserializeObject<dynamic>(model);
                //If there is a registered game with available white cards, we must also add this to that deck.
                var gameId = dmodel?.GameId as string;
                var text = dmodel?.Text;
                _cardService.CreateCustomWhiteCard(gameId, id, text);
                _gameService.GetGame(gameId)?.AddCustomWhiteCard(id);
                return id;
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<WhiteCardModel>> GetHand(string gameId, int roundNumber, string playerId)
        {
            return await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                var hand = game.Hands[playerId];
                var cards = new List<WhiteCardModel>();
                hand.CardsInHand.AsParallel().ForAll(k =>
                {
                    var whiteCard = _cardService.GetWhiteCard(gameId, k);
                    cards.Add(new WhiteCardModel() {Value = whiteCard.Value, WhiteCardId = whiteCard.WhiteCardId});
                });
                return cards;
            });
        }
    }
}

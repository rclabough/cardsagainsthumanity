﻿using CardsAgainstHumanity.Server.Logic.Game;
using CardsAgainstHumanity.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using CardsAgainstHumanity.Shared.Endpoints;

namespace CardsAgainstHumanity.Server.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GameController : ApiController, IGameEndpoint
    {
        private readonly GameService _gameService;

        public GameController(GameService gameService)
        {
            _gameService = gameService;
        }

        [HttpGet]
        public async Task Start(string gameId)
        {
            await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                game.Start();
            });
        }

        [HttpGet]
        public async Task<IEnumerable<string>> Players(string gameId)
        {
            return await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                return game.Players;
            });
        }

        [HttpGet]
        public async Task EndGame(string gameId)
        {
            await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                game.End();
            });
        }

        [HttpGet]
        public async Task<GameScoreModel> Scores(string gameId)
        {
            return await Task.Run(() =>
            {
                var game = _gameService.GetGame(gameId);
                return new GameScoreModel
                {
                    Scores = game.GetScores().Select(k => new ScoreModel
                    {
                        PlayerId = k.Key,
                        Score = k.Value
                    }).ToList()
                };
            });
        }
    }
}

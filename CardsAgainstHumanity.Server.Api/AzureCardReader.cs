﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using CardsAgainstHumanity.Server.Data;
using CardsAgainstHumanity.Server.Data.Cards;
using CardsAgainstHumanity.Shared.Extensions;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;

namespace CardsAgainstHumanity.Server.Api
{
    public class AzureCardReader : ICardReader
    {
        private readonly string _azureConnectionString;
        private readonly CloudFile _whiteCardFile;
        private readonly CloudFile _blackCardFile;

        public AzureCardReader()
        {
            _azureConnectionString = ConfigurationManager.AppSettings["StorageConnectionString"];
        }


        private async Task<CloudFile> GetFile(string dir, string fileName)
        {
            var storageAccount = CloudStorageAccount.Parse(_azureConnectionString);
            var fileClient = storageAccount.CreateCloudFileClient();
            // Get a reference to the file share we created previously.
            CloudFileShare share = fileClient.GetShareReference("cardfiles");
            if (await share.ExistsAsync())
            {
                CloudFileDirectory rootDir = share.GetRootDirectoryReference();

                // Get a reference to the directory we created previously.
                CloudFileDirectory sampleDir = rootDir.GetDirectoryReference(dir);
                if (await sampleDir.ExistsAsync())
                {
                    // Get a reference to the file we created previously.
                    CloudFile file = sampleDir.GetFileReference(fileName);

                    // Ensure that the file exists.
                    if (file.Exists())
                    {
                        return file;
                    }
                }
            }
            return null;
        }


        public async Task<IEnumerable<WhiteCard>> GetWhiteCardsAsync()
        {
            var file = await GetFile("cards", "WhiteCards.txt");
            var text = await file.DownloadTextAsync();
            return text.Split('\n').Select(k => 
                new WhiteCard { Value = k, WhiteCardId = Guid.NewGuid() }).ToConcurrentBag();
        }

        public async Task<IEnumerable<BlackCard>> GetBlackCardsAsync()
        {
            var file = await GetFile("cards", "BlackCards.txt");
            var text = await file.DownloadTextAsync();
            return text.Split('\n').Select(k =>
                new BlackCard {RawValue = k, BlackCardId = Guid.NewGuid()})
                .ToConcurrentBag();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CardsAgainstHumanity.Shared;
using CardsAgainstHumanity.Shared.Exceptions;
using Newtonsoft.Json;

namespace CardsAgainstHumanity.Plug
{
    public class Portal
    {
        public static Portal Instance => _instance ?? (_instance = new Portal());
        public static string BaseUri => AppConfigurationSettings.Instance.ApiUrl;
        private static Portal _instance;
        private Portal()
        {
            
        }
        

        public async Task<T> Get<T>(string uri, dynamic model = null)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var paramUri = model == null ? uri : GetQueryString(uri, model);

                    var cancel = new CancellationToken();
                    var response = await client.GetAsync(paramUri, cancel);
                    response.EnsureSuccessStatusCode();
                    var responseContent = await response.Content.ReadAsStringAsync();
                    return ValidateOrThrow<T>(responseContent);
                }
                catch (Exception ex)
                {
                    throw new AggregateException(ex);
                }
                
            }
        }

        private string GetQueryString(string uri, dynamic model)
        {
            var ser1 =JsonConvert.SerializeObject(model);
            Dictionary<string, object> dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(ser1);
            
            if (dict == null || !dict.Any()) return uri;
            var sb = new StringBuilder(uri);
            var first = true;
            foreach (var param in dict)
            {
                if (first)
                {
                    sb.Append("?");
                    first = false;
                }
                else
                {
                    sb.Append("&");
                }
                sb.Append(param.Key + "=" + param.Value);
            }
            return sb.ToString();
        }

        public async Task<S> Post<S>(string uri, dynamic model)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var ser = JsonConvert.SerializeObject(model);
                    var content = new StringContent(ser)
                    {
                        Headers = {ContentType = new MediaTypeHeaderValue("application/json")}
                    };
                    var response = await client.PostAsync(uri, content).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    var responseContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    return ValidateOrThrow<S>(responseContent);
                }
                catch (Exception ex)
                {
                    throw new AggregateException(ex);
                }
            }
        }

        private T ValidateOrThrow<T>(string response)
        {
            return JsonConvert.DeserializeObject<T>(response);
        }
    }
}

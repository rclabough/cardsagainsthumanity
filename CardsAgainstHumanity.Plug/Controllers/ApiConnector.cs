﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CardsAgainstHumanity.Plug.Attribute;
using CardsAgainstHumanity.Shared;

namespace CardsAgainstHumanity.Plug.Controllers
{
    public class ApiConnector
    {
        private readonly string _basePath = AppConfigurationSettings.Instance.ApiUrl;


        protected void Call(dynamic p = null)
        {
            Call<object>(p);
        }

        protected T Call<T>(dynamic p = null)
        {
            return Task.Run(() => CallAsync<T>(p)).Result;
        }

        protected async Task CallAsync(dynamic p = null)
        {
            await CallAsync<object>(p);
        }

        protected async Task<T> CallAsync<T>(dynamic p = null)
        {
            var portal = Portal.Instance;
            var prop = (string)typeof (Environment).GetRuntimeProperty("StackTrace").GetValue(null);
            //var stackTrace = Environment.StackTrace;

            var frames = prop.Split('\n').ToList();

            var list = new StackContainer[frames.Count];
            StackContainer prev = null;
            for (var i = frames.Count-1; i >= 0; i--)
            {
                list[i] = new StackContainer
                {
                    Id = i,
                    Next = prev,
                    StackValue = frames[i]
                };
                prev = list[i];
            }
            var callingFrame =
                list.First(
                    k =>
                        k.StackValue.Contains("ApiConnector.Call") && k.Next != null && k.Next.StackValue.Contains("<") &&
                        k.Next.StackValue.Contains(">")).Next.StackValue;

            //at CardsAgainstHumanity.Plug.Controllers.MatchApi.CreateGame()[0x00001] in C: \Users\rober_000\documents\visual studio 2015\Projects\CardsAgainstHumanity\CardsAgainstHumanity.Plug\Controllers\MatchApi.cs:17
            if (callingFrame.Contains("<"))
            {
                callingFrame = callingFrame.Substring(callingFrame.IndexOf("<") + 1);
                callingFrame = callingFrame.Substring(0, callingFrame.IndexOf(">"));
            }
            else
            {
                callingFrame = callingFrame.Substring(callingFrame.IndexOf("at") + 2);
                callingFrame = callingFrame.Substring(0, callingFrame.IndexOf('['));
                callingFrame = callingFrame.Split('.').Last();
                callingFrame = callingFrame.Substring(0, callingFrame.IndexOf('(')).Trim();
            }
            var controllerPathAttribute = GetType().GetTypeInfo().GetCustomAttribute<ControllerPathAttribute>();
            var controllerPath = controllerPathAttribute?.ControllerPath ?? string.Empty;

            //StackTrace
            var method = GetType().GetTypeInfo().GetDeclaredMethod(callingFrame).GetCustomAttribute<MethodPathAttribute>();


            var path = _basePath + controllerPath + @"/" + method.MethodPath;


            switch (method.MethodType)
            {
                case MethodHttpType.Get:
                    return await portal.Get<T>(path, p);
                case MethodHttpType.Post:
                    return await portal.Post<T>(path, p);
            }

            return default(T);
        }


        private class StackContainer
        {
            public int Id { get; set; }
            public string StackValue { get; set; }
            public StackContainer Next { get; set; }

        }
    }


}

﻿using System.Threading.Tasks;
using CardsAgainstHumanity.Plug.Attribute;
using CardsAgainstHumanity.Shared.Endpoints;
using CardsAgainstHumanity.Shared.Models;

namespace CardsAgainstHumanity.Plug.Controllers
{

    [ControllerPath("Match")]
    public class MatchApi : ApiConnector, IMatchEndpoint
    {
        [MethodPath("CreateGame", MethodHttpType.Get)]
        public async Task<string> CreateGame()
        {

            return await CallAsync<string>();
        }

        [MethodPath("JoinGame", MethodHttpType.Post)]
        public async Task<bool> JoinGame(JoinGameModel model)
        {
            return await CallAsync<bool>(new {gameId = model.GameId, playerId = model.PlayerId});
        }

        [MethodPath("LeaveGame", MethodHttpType.Get)]
        public async Task<bool> LeaveGame(string playerId, string gameId)
        {
            return await CallAsync<bool>(new
            {
                playerID = playerId,
                gameId = gameId
            });
        }
    }
}

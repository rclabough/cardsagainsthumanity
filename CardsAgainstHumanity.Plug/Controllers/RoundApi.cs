﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CardsAgainstHumanity.Plug.Attribute;
using CardsAgainstHumanity.Shared.Endpoints;
using CardsAgainstHumanity.Shared.Models;

namespace CardsAgainstHumanity.Plug.Controllers
{
    [ControllerPath("Round")]
    public class RoundApi : ApiConnector, IRoundEndpoint
    {
        [MethodPath("Create", MethodHttpType.Get)]
        public async Task<int> Create(string gameId)
        {
            return await CallAsync<int>(new {gameId = gameId});
        }

        [MethodPath("GetPlayerRound", MethodHttpType.Get)]
        public async Task<PlayerRoundModel> GetPlayerRound(string gameId, string playerId)
        {
            return await CallAsync<PlayerRoundModel>(new {gameId = gameId, playerId = playerId});
        }

        [MethodPath("PlayersWhoSubmitted", MethodHttpType.Get)]
        public async Task<IEnumerable<string>> PlayersWhoSubmitted(string gameId)
        {
            return await CallAsync<IEnumerable<string>>(new {gameId = gameId});
        }

        [MethodPath("Submissions", MethodHttpType.Get)]
        public async Task<SubmissionModel> Submissions(string gameId)
        {
            return await CallAsync<SubmissionModel>(new {gameId = gameId});
        }

        [MethodPath("GetHostRound", MethodHttpType.Get)]
        public async Task<HostRoundModel> GetHostRound(string gameId)
        {
            return await CallAsync<HostRoundModel>(new {gameId = gameId});
        }

        [MethodPath("IsRoundOver", MethodHttpType.Get)]
        public async Task<bool> IsRoundOver(string gameId)
        {
            return await CallAsync<bool>(new {gameId = gameId});
        }

        [MethodPath("End", MethodHttpType.Get)]
        public async Task End(string gameId)
        {
            await CallAsync(new {gameId = gameId});
        }

        [MethodPath("Submit", MethodHttpType.Get)]
        public async Task Submit(SubmitCardModel model)
        {
            await CallAsync(new {model = model});
        }

        [MethodPath("SubmitWinner", MethodHttpType.Post)]
        public async Task SubmitWinner(PlayerModel model)
        {
            await CallAsync(new {model = model});
        }
    }
}

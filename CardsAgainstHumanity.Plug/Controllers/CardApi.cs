﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CardsAgainstHumanity.Plug.Attribute;
using CardsAgainstHumanity.Shared.Endpoints;
using CardsAgainstHumanity.Shared.Models;

namespace CardsAgainstHumanity.Plug.Controllers
{
    [ControllerPath("Card")]
    public class CardApi : ApiConnector, ICardEndpoint
    {
        [MethodPath("BlackCard", MethodHttpType.Get)]
        public async Task<BlackCardModel> BlackCard(string gameId, int roundNumber)
        {
            return await CallAsync<BlackCardModel>(new {gameId = gameId, roundNumber = roundNumber});
        }

        [MethodPath("SubmitCustomCard", MethodHttpType.Post)]
        public async Task<Guid> SubmitCustomCard(string model)
        {
            return await CallAsync<Guid>(new {model = model});
        }

        [MethodPath("GetHand", MethodHttpType.Get)]
        public async Task<IEnumerable<WhiteCardModel>> GetHand(string gameId, int roundNumber, string playerId)
        {
            return
                await
                    CallAsync<IEnumerable<WhiteCardModel>>(
                        new {gameId = gameId, roundNumber = roundNumber, playerId = playerId});
        }
    }
}

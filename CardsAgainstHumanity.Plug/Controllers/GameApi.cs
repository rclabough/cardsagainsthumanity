﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CardsAgainstHumanity.Plug.Attribute;
using CardsAgainstHumanity.Shared.Endpoints;
using CardsAgainstHumanity.Shared.Models;

namespace CardsAgainstHumanity.Plug.Controllers
{
    [ControllerPath("Game")]
    public class GameApi : ApiConnector, IGameEndpoint
    {
        [MethodPath("Start", MethodHttpType.Get)]
        public async Task Start(string gameId)
        {
            await CallAsync(new {gameId = gameId});
        }

        [MethodPath("Players", MethodHttpType.Get)]
        public async Task<IEnumerable<string>> Players(string gameId)
        {
            return await CallAsync<IEnumerable<string>>(new {gameId = gameId});
        }

        [MethodPath("EndGame", MethodHttpType.Get)]
        public async Task EndGame(string gameId)
        {
            await CallAsync(new {gameId = gameId});
        }

        [MethodPath("Scores", MethodHttpType.Get)]
        public async Task<GameScoreModel> Scores(string gameId)
        {
            return await CallAsync<GameScoreModel>(new {gameId = gameId});
        }
    }
}

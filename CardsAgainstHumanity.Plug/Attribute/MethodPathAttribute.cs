﻿using System.Net.Http;

namespace CardsAgainstHumanity.Plug.Attribute
{
    public class MethodPathAttribute : System.Attribute
    {
        public string MethodPath { get; private set; }
        public MethodHttpType MethodType { get; private set; }

        public MethodPathAttribute(string path, MethodHttpType methodType)
        {
            MethodType = methodType;
            MethodPath = path;
        }

    }

    public enum MethodHttpType
    {
        Get,
        Post,
        Put
    }
}

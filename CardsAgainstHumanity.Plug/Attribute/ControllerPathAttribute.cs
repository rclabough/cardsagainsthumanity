﻿namespace CardsAgainstHumanity.Plug.Attribute
{
    public class ControllerPathAttribute : System.Attribute
    {
        public string ControllerPath { get; private set; }

        public ControllerPathAttribute(string path)
        {
            ControllerPath = path;
        }
    }
}

﻿namespace CardsAgainstHumanity.Server.Api.Endpoints
{
    public interface IMatchEndpoint
    {
        string CreateGame();
        bool JoinGame(JoinGameModel model);
    }
}

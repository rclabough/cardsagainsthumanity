﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardsAgainstHumanity.Shared;
using Microsoft.AspNet.SignalR.Client;

namespace CardsAgainstHumanity.Plug.SignalR
{
    public class SignalRClient
    {
        private SignalRClient()
        {
            
        }

        public delegate void PlayerAddedHandler(string playerId);
        public delegate void GameReadyHandler(string message);
        public delegate void PlayerSubmittedHandler(string playerId);
        public delegate void RoundOverHandler();
        public delegate void GameEndedHandler(string gameId);


        public event PlayerAddedHandler PlayerAdded;
        public event GameReadyHandler GameReady;
        public event PlayerSubmittedHandler PlayerSubmitted;
        public event RoundOverHandler RoundOver;
        public event GameEndedHandler GameEnded;

        public static async Task<SignalRClient> GetInstance(string gameId)
        {
            var instance = new SignalRClient();
            await instance.Connect(gameId);
            return instance;
        }

        private async Task Connect(string gameId)
        {
            var hubConnection = new HubConnection(AppConfigurationSettings.Instance.SignalRUrl);

            // Create a proxy to the 'ChatHub' SignalR Hub
            var chatHubProxy = hubConnection.CreateHubProxy("Player");

            // Wire up a handler for the 'UpdateChatMessage' for the server
            // to be called on our client
            chatHubProxy.On<string>("playerAdded", playerId => PlayerAdded?.Invoke(playerId));
            chatHubProxy.On<string>("gameReady", message => GameReady?.Invoke(message));
            chatHubProxy.On<string>("playerSubmitted", playerId => PlayerSubmitted?.Invoke(playerId));
            chatHubProxy.On<object>("roundOver", (isnothing) => RoundOver?.Invoke());
            chatHubProxy.On<string>("gameEnded", playerId => GameEnded?.Invoke(playerId));
            // Start the connection
            await hubConnection.Start();

            // Invoke the 'UpdateNick' method on the server
            await chatHubProxy.Invoke("Subscribe", gameId);
        }
        
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardsAgainstHumanity.Shared
{
    public class AppConfigurationSettings
    {
        public string BaseUrl { get; private set; }
        public string SignalRUrl { get; private set; }
        public string ApiUrl { get; private set; }
        public static AppConfigurationSettings Instance => new AppConfigurationSettings();
        private AppConfigurationSettings()
        {
#if DEBUG
            BaseUrl = "http://cardsagainsthumanity1403.azurewebsites.net/";
#else
            BaseUrl = "http://cardsagainsthumanity1403.azurewebsites.net/";
#endif
            //BaseUrl = "http://localhost:63118/";
            SignalRUrl = BaseUrl + "signalr/hubs";
            ApiUrl = BaseUrl + "api/";
        }
    }
}

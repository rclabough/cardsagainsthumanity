﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardsAgainstHumanity.Shared.Models
{
    public class JoinGameModel
    {
        public string GameId { get; set; }
        public string PlayerId { get; set; }
    }
}

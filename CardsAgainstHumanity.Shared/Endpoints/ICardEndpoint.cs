﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CardsAgainstHumanity.Shared.Models;

namespace CardsAgainstHumanity.Shared.Endpoints
{
    public interface ICardEndpoint
    {
        Task<BlackCardModel> BlackCard(string gameId, int roundNumber);
        Task<Guid> SubmitCustomCard(string model);
        Task<IEnumerable<WhiteCardModel>> GetHand(string gameId, int roundNumber, string playerId);
    }
}

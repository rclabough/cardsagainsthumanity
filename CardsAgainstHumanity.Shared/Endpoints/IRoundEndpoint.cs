﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CardsAgainstHumanity.Shared.Models;

namespace CardsAgainstHumanity.Shared.Endpoints
{
    public interface IRoundEndpoint
    {
        Task<int> Create(string gameId);
        Task<PlayerRoundModel> GetPlayerRound(string gameId, string playerId);
        Task<IEnumerable<string>> PlayersWhoSubmitted(string gameId);
        Task<SubmissionModel> Submissions(string gameId);
        Task<HostRoundModel> GetHostRound(string gameId);
        Task<bool> IsRoundOver(string gameId);
        Task End(string gameId);
        Task Submit(SubmitCardModel model);
        Task SubmitWinner(PlayerModel model);
    }
}

﻿using System.Threading.Tasks;
using CardsAgainstHumanity.Shared.Models;

namespace CardsAgainstHumanity.Shared.Endpoints
{
    public interface IMatchEndpoint
    {
        Task<string> CreateGame();
        Task<bool> JoinGame(JoinGameModel model);
        Task<bool> LeaveGame(string playerId, string gameId);
    }
}

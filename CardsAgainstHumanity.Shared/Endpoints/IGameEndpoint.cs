﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CardsAgainstHumanity.Shared.Models;

namespace CardsAgainstHumanity.Shared.Endpoints
{
    public interface IGameEndpoint
    {
        Task Start(string gameId);
        Task<IEnumerable<string>> Players(string gameId);
        Task EndGame(string gameId);
        Task<GameScoreModel> Scores(string gameId);
    }
}

﻿namespace CardsAgainstHumanity.Shared.GameProperties
{
    public class GameProperties
    {
        public static GameProperties Instance { get; } = new GameProperties();

        private GameProperties()
        {
            
        }


        public string GameId { get; set; }
        public string PlayerId { get; set; }
    }
}

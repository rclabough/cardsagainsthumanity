﻿(function () {
    var app = angular.module('cah');
    app.controller('RoundHostCtrl', function ($scope, $location, apiservice, gameproperties, signalrservice, signalrhubs) {
        $scope.GameId = gameproperties.getGameId();
        $scope.CountdownEnabled = true;
        $scope.RoundOver = false;
        $scope.MaxTime = parseInt(gameproperties.getMaxTime());
        $scope.RemainingSeconds = $scope.MaxTime;

        var allplayerssubmitted = function() {
            var all = true;
            for (var i in $scope.Players) {
                all = all && $scope.Players[i];
            }
            if (all) {
                $scope.RoundOver = all;
            }
            return all;
        }

        $scope.ChangeMaxTime = function() {
            gameproperties.setMaxTime($scope.MaxTime);
        }

        $scope.completeRound = function() {
            apiservice.CompleteRound(gameproperties.getGameId(), function() {
                $location.path('/pickwinner');
            });
        }


        var countdown = function () {
            var refreshId = setInterval(function () {
                if ($scope.CountdownEnabled) {
                    if ($scope.RemainingSeconds <= 0) {
                        clearInterval(refreshId);
                        $scope.completeRound();
                    } else {
                        $scope.RemainingSeconds = $scope.RemainingSeconds - 1;
                        var percent = $scope.RemainingSeconds / $scope.MaxTime;
                        if (percent > 0.5) {
                            $scope.PBType = 'success';
                        } else if (percent > 0.15) {
                            $scope.PBType = 'warning';
                        } else {
                            $scope.PBType = 'danger';
                        }
                        
                        $scope.$apply();
                    }
                }
            }, 1000);
        }

        var startround = function () {
            $scope.RoundOver = false;
            apiservice.CreateRound(gameproperties.getGameId(), function(roundNumber) {
                $scope.RoundNumber = roundNumber;
                apiservice.GetHostRound($scope.GameId, function(result) {
                    $scope.Players = {};
                    for (var i = 0; i < result.Players.length; i++) {
                        var player = result.Players[i];
                        $scope.Players[player] = false;
                    }
                    $scope.BlackCard = result.BlackCard;
                }, function(error) {
                    console.log(error);
                });
                if ($scope.CountdownEnabled) {
                    countdown();
                }
            }, function(error) { console.log(error); });
        }

        signalrservice.Initialize(function () {
            signalrhubs.setOnPlayerSubmitted(function (playeraddedid) {
                $scope.Players[playeraddedid] = true;
                allplayerssubmitted();
                $scope.$apply();
            });
            
        });
        startround();
    });

    app.controller('RoundCtrl', function ($scope, $location, gameproperties, apiservice, signalrservice, signalrhubs) {
        $scope.Items = [];
        $scope.Picks = [];
        $scope.HasSubmitted = false;
        $scope.RoundOver = false;
        $scope.PlayersWhoSubmitted = [];

        if (!gameproperties.getGameId()) {
            gameproperties.setRedirect('/round');
            $location.path('/');
        }

        $scope.GetPicks = function () {
            var arr = [$scope.BlackCard.Pick];
            for (var i = 0; i < $scope.BlackCard.Pick; i++){
                arr[i] = i;
            }
            return arr;
        };

        function format(str) {
            for (var i = 0; i < $scope.BlackCard.Pick; i++) {
                var symbol = '{' + i + '}';
                var pick = $scope.Picks[i];
                if (!pick) {
                    str = str.replace(symbol, '____');
                } else {
                    str = str.replace(symbol, pick.card.Value);
                }
                
            }
            return str;
        }


        function getFromId(whiteCardId) {
            return $scope.Items.filter(k => k.card).filter(k => k.card.WhiteCardId === whiteCardId)[0];
        }

        $scope.Submit = function () {
            var cardIds = [];
            for (var i = 0; i < $scope.Picks.length; i++) {
                cardIds[i] = $scope.Picks[i].card.WhiteCardId;
            }
            apiservice.SubmitCard(gameproperties.getGameId(), gameproperties.getPlayerId(), cardIds, function (result) {
                $scope.HasSubmitted = true;
                $location.path('/loading');
                //Do something while waiting
            }, function (error) {
                console.log(error);
            });
        }

        $scope.Select = function (id, pick) {
            var chosen = getFromId(id);
            $scope.Picks[pick] = chosen;
            $scope.Readout = format($scope.BlackCard.FormattableValue);
        }

        $scope.StylePicks = function() {
            $scope.Items.each(k1 => k1.css = '');
            $scope.Picks.each(k1 => k1.css = 'picked');
        }

        apiservice.GetPlayerRound(gameproperties.getGameId(), gameproperties.getPlayerId(), function (result) {
            $scope.WhiteCards = result.WhiteCards;
            var count = 0;
            $scope.Items = $scope.WhiteCards.map(function(k) {
                return {
                    card: k,
                    css: '',
                    index: count++
                }
            });
            $scope.BlackCard = result.BlackCard;
            $scope.RoundNumber = result.RoundNumber;
            $scope.Readout = $scope.BlackCard.BlankValue;
        }, function (error) { });

        signalrservice.Initialize(function () {
            signalrhubs.setOnPlayerSubmitted(function (playeraddedid) {
                $scope.PlayersWhoSubmitted.push(playeraddedid);
            });
            signalrhubs.setOnRoundOver(function () {
                console.log('Round is over');
                $scope.RoundOver = true;
            });
        });
    });
})();